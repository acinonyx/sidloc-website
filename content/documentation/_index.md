---
title: "Documentation"
date: 2021-07-12T22:38:43+03:00
---

## Documents ##

{{<toc-tree>}}

## Platforms ##

### Specifications, Design, and Experiment Documents ###

For the creation, collaboration, and management of detailed documents such as requirements specifications, verification plans, design documents, and experiment reports, we use [Overleaf](https://www.overleaf.com/).
Overleaf provides a cloud-based LaTeX editing environment, enabling team collaboration, version control, and real-time updates.

### Public Documentation ###

Public-facing documentation that is intended for broader consumption (e.g., external audiences, clients, or open-source communities) is hosted on the [SIDLOC website](https://sidloc.org/).
The website serves as an organized and easily accessible portal for public documentation, facilitating transparency and knowledge sharing.

### Per Project Documentation ###

Each project may have its own documentation that is more specific to the codebase or technical implementation.
Depending on the project type, the following platforms are used for per-project documentation:

- **README file**: For smaller projects or repositories where a simple, concise overview suffices, a `README.md` file is used to provide essential information.
- **Sphinx**: For Python-based projects or those requiring more sophisticated documentation, [Sphinx](https://www.sphinx-doc.org/) is utilized to generate structured, extensible documentation from reStructuredText files.
- **Doxygen**: For projects involving C, C++, or other languages in which automatically generating detailed code-level documentation is more suitable, [Doxygen](https://www.doxygen.nl/) is employed.

This structure ensures that all key phases of the project lifecycle—requirements, design, implementation, and public dissemination—are appropriately documented using tools suited to the specific document type.
