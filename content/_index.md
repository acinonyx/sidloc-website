---
title: "SIDLOC"
date: 2021-06-30T12:03:36+03:00
---

SIDLOC (Spacecraft Identification and Localization) is a proposed standard for satellite and spacecraft identification and localization.
It is developed as an [ESA](https://esa.int) funded, [ARTES](https://artes.esa.int) activity by [Libre Space Foundation](https://libre.space) and [FORTH](https://www.forth.gr).

This website will be updated with more information as the design and development activities progress.
